#
# Local bash env settings
#
export TT8INCLUDE=/usr/mlf2/include
export TT8LIBDIR=/usr/mlf2/lib
export HISTSIZE=2500
export HISTFILESIZE=2500
export HISTTIMEFORMAT='%Y-%m-%d %H:%M:%S '
export IGNOREEOF=10
export FIGNORE=".o:~:.orig"
export TIME_STYLE=long-iso

if [ -f $HOME/.git-completion.bash ]
then
    source $HOME/.git-completion.bash
    PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '
fi

if [ -e /dev/ttyS0 ]
then
    export PICODOS_SERIAL="/dev/ttyS0"
    export DEFAULT_SERIAL="/dev/ttyS0"
fi

complete -f -c sudo

# correct minor errors in 'cd' commands
shopt -s cdspell
# good for double-checking history substitutions
shopt -s histverify
shopt -s histappend

shopt -s no_empty_cmd_completion

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

