Virtual Machine for MHK development
===================================

This repository contains the Vagrant configuration to create a
Debian 8 based VirtualBox VM for MHK development.

Prerequisites
-------------

 - VirtualBox v5+
 - Vagrant v1.7+

Shared Directories
------------------

You can customize the directories shared between the Host and Guest
systems by editing the file *vmshares*. This is an ASCII file with
two columns of data. The first column is the relative directory on
the Host and the second is the corresponding directory on the Guest.
